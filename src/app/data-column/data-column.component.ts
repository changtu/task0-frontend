import { AlfrescoApiService, DataColumnComponent } from '@alfresco/adf-core';
import { NodesApi } from '@alfresco/js-api';
import { from, Observable } from 'rxjs';
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /* tslint:disable:component-selector no-input-rename  */

import { ChangeDetectionStrategy, Component, ContentChild, ElementRef, Input, OnInit, TemplateRef, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-data-column',
    template: '',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    // host: { class: 'adf-datatable-content-cell adf-name-column' },
})
export class MyDataColumnComponent extends DataColumnComponent implements OnInit {

    /** Data source key. Can be either a column/property key like `title`
     *  or a property path like `createdBy.name`.
     */
    @Input()
    key: string;

    /** Value type for the column. Possible settings are 'text', 'image',
     * 'date', 'fileSize', 'location', and 'json'.
     */
    @Input()
    type: string = 'text';

    /** Value format (if supported by the parent component), for example format of the date. */
    @Input()
    format: string;

    /** Toggles ability to sort by this column, for example by clicking the column header. */
    @Input()
    sortable: boolean = true;

    /** Display title of the column, typically used for column headers. You can use the
     * i18n resource key to get it translated automatically.
     */
    @Input()
    title: string = '';

    @ContentChild(TemplateRef)
    template: any;

    /** Custom tooltip formatter function. */
    @Input()
    formatTooltip: Function;

    /** Title to be used for screen readers. */
    @Input('sr-title')
    srTitle: string;

    /** Additional CSS class to be applied to column (header and cells). */
    @Input('class')
    cssClass: string;

     /** Enables/disables a Clipboard directive to allow copying of cell contents. */
    @Input()
    copyContent: boolean;

    /**  Toggles the editing support of the column data. */
    @Input()
    editable: boolean = false;

    /**  Enable or disable cell focus */
    @Input()
    focus: boolean = true;

    /** When using server side sorting the column used by the api call where the sorting will be performed */
    @Input()
    sortingKey: string;

    /** Data column header template */
    header?: TemplateRef<any>;

    count : Observable<number | string> = null;
    // count : Observable<number | string> = null;

    constructor(public elementRef: ElementRef, private apiService: AlfrescoApiService) {
      super();
    }

    ngOnInit() {

      console.log('id ' + '$id');
      this.count = new Observable(observer => {
          observer.next("Loading...");
      });

      if (!this.srTitle && this.key === '$thumbnail') {
          this.srTitle = 'Thumbnail';
      }


      this.count = from(this.countFolder('$id'));
      // get node id

      console.log('id ' + '$id');


    }

    // onClick(event: Event) {
    //   console.log('onClick');
    // }

    async countFolder(folder : string) : Promise<number> {
      let count : number = 0;


      let curFolder = folder;

      // var nodesApi = new NodesApi(alfrescoApi);
      var nodesApi = new NodesApi(this.apiService.getInstance());

      await nodesApi.listNodeChildren(curFolder).then( async (data) => {
        console.log('API called successfully. Returned data: ' + data);
        // console.log(data);
        console.log(data.list.entries);
        // todo pagination?
        // totalCount

        // the number of files of all levels in the folder
        let nFiles : number = 0;
        for (var i : number = 0; i < data.list.entries.length; i++) {
          if (data.list.entries[i].entry.isFile) {
            nFiles++;
            // console.log(nFiles+"+1");
          }else if(data.list.entries[i].entry.isFolder){
            nFiles += await this.countFolder(data.list.entries[i].entry.id);
          }else{
            // console.log(data.list.entries[i].entry.isFile);
          }
          // console.log(i);
        }

        count = nFiles;

      }, (error) => {
        console.log('Error', error);
      });




      return count;

    }
}
