import { ComponentFixture } from '@angular/core/testing';
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MyDataColumnComponent } from './data-column.component';
// import { setupTestBed } from '../testing/setup-test-bed';
import { TranslateModule } from '@ngx-translate/core';
import { CoreTestingModule, setupTestBed } from '@alfresco/adf-core/testing';
import { AlfrescoApiService } from '@alfresco/adf-core';

describe('DataColumnListComponent', () => {

  let component: MyDataColumnComponent;
  let fixture: ComponentFixture<MyDataColumnComponent>;
  let apiService: AlfrescoApiService;

  // describe('DocumentsComponent', () => {


  //   const notificationServiceMock = {
  //     openSnackMessage: jasmine.createSpy('openSnackMessage')
  // };

    setupTestBed({
        imports: [
            TranslateModule.forRoot(),
            CoreTestingModule
        ]
    });

    it('should setup screen reader title for thumbnails', () => {
        const component = new MyDataColumnComponent(apiService);
        component.key = '$thumbnail';
        expect(component.srTitle).toBeFalsy();
        component.ngOnInit();
        expect(component.srTitle).toBeTruthy();
    });
});


