import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';


export interface NodeCounts {
  nodeId: string;
  filesCount: number;
  foldersCount: number;
}

@Injectable({
  providedIn: 'root'
})
export class CountingChildrenService {



  // countsOfFiles : {} = {};
  configUrl = 'http://localhost:8080/alfresco/s/task0/countingfiles';
  // configUrl = '0';

  constructor(private http: HttpClient) { }



  countFiles(folderId : string) : Observable<NodeCounts> {

    // debugger;
    // the number of files of all levels in the folder
    let nFiles : number = 0;
    let nFolders : number = 0;
    console.log("curFolder: " + folderId);
    const params :HttpParams = new HttpParams().set('id', folderId);
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
    const options = {
      headers: headers,
      params: params ,
      responseType: 'json' as const
    };
    return this.http.get<NodeCounts>(this.configUrl, options);
  }
}
