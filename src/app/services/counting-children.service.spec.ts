import { TestBed } from '@angular/core/testing';

import { CountingChildrenService } from './counting-children.service';

describe('CountingChildrenService', () => {
  let service: CountingChildrenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CountingChildrenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
