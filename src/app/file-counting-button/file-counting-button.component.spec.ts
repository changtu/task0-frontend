import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileCountingButtonComponent } from './file-counting-button.component';

describe('FileCountingButtonComponent', () => {
  let component: FileCountingButtonComponent;
  let fixture: ComponentFixture<FileCountingButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileCountingButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileCountingButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
