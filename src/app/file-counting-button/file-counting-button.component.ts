import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-file-counting-button',
  templateUrl: './file-counting-button.component.html',
  styleUrls: ['./file-counting-button.component.css']
})
export class FileCountingButtonComponent implements OnInit {

  @Input()
  nodeId : string = null;

  @Output()
  numberOfFiles: number = 0;

  constructor() { }

  ngOnInit(): void {

  }

}
