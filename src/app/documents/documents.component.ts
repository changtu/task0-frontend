import { NodesApi } from '@alfresco/js-api';
import { CountingChildrenService, NodeCounts } from './../services/counting-children.service';
import { Output, OnInit, AfterViewInit } from '@angular/core';


/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Component, ViewChild, Input } from '@angular/core';
import { AlfrescoApiService, NotificationService } from '@alfresco/adf-core';
import { DocumentListComponent } from '@alfresco/adf-content-services';
import { PreviewService } from '../services/preview.service';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss'],
  providers: [CountingChildrenService]
})
export class DocumentsComponent implements OnInit, AfterViewInit {

  @Input()
  showViewer = false;
  nodeId: string = null;

  @ViewChild('documentList')
  documentList: DocumentListComponent;

  countsOfFiles : {} = {};
  countsOfFolders: {} = {};
  count: number = 0;
  nodesApi: NodesApi;

  constructor(
    private notificationService: NotificationService,
    private preview: PreviewService,
    private countingChildrenService: CountingChildrenService,
    private apiService: AlfrescoApiService,
  ) {
    this.nodesApi = new NodesApi(this.apiService.getInstance());
  }
  ngAfterViewInit(): void {
    // console.log("ngAfterViewInit doc", this.documentList);
    this.getFolderCounts(this.documentList.currentFolderId);
  }


  ngOnInit() {
    // console.log('ngOnInit' );
    // console.log(this.documentList);
  }


  uploadSuccess(event: any) {
    this.notificationService.openSnackMessage('File uploaded');
    this.documentList.reload();
  }

  showPreview(event) {
    console.log(event);
    // debugger;
    const entry = event.value.entry;
    if (entry && entry.isFile) {
      this.preview.showResource(entry.id);
    }
  }

  onGoBack(event: any) {
    this.showViewer = false;
    this.nodeId = null;
  }

  onClickCountingChildren(event: any){
    console.log("doc", this.documentList);
    console.log("event", event);
    // this.documentList.currentFolderId
    this.getFolderCounts(this.documentList.folderNode.id);
  }

  getFolderCounts(folderId: string) : void{
    console.log('getting counts for root folder: ' + folderId);

    this.nodesApi.listNodeChildren(folderId).then(
      (data) => {
        console.log('getting children of folder: ' + folderId);
        console.log(data);

        data.list.entries.forEach(element => {
          console.log("element", element);
          let childId = element.entry.id;
          this.countingChildrenService.countFiles(childId).subscribe(
            (response: NodeCounts) => {
              console.log('got counts for folder: ' + childId);
              console.log(response);
              this.countsOfFiles[childId] = response.filesCount;
              this.countsOfFolders[childId] = response.foldersCount;
            }
          )
        }
      );

      }
    );
  }
}
